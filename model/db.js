var mysql = require('mysql');

var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "dsmDB"
});

//var con = mysql.createConnection(process.env.CLEARDB_DATABASE_URL);

var sqlV = " CREATE TABLE IF NOT EXISTS visiteurs ( id BIGINT(20) NULL , nom VARCHAR(255) NULL , prenom VARCHAR(255) NULL , message VARCHAR(255) NULL , ip VARCHAR(255) NULL , pays VARCHAR(255) NULL , navigateur VARCHAR(255) NULL ,    date VARCHAR(255) NULL )";

var sqlP = "CREATE TABLE IF NOT EXISTS pdf (idvisiteur BIGINT(20) NULL, urlpdf VARCHAR(255) NULL)";

con.connect(function(err) {

    if(!err){
        console.log('Connexion à mysql effectue avec succes');

        con.query(sqlV,function (err, result) {
            if (err) {  console.log("création de la table visiteur"+err);   }
        });

        con.query(sqlP,function (err, result) {
            if (err) {  console.log("création de la table pdf"+err);   }
        });

    }else{
        console.log('Echec de connexion a mysql : ' + err)
    }
});



