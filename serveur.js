//require('./model/db');

const express = require('express');
const path = require('path');
const expresshand = require('express-handlebars');
const bodyparser = require('body-parser');

const mainController = require('./controllers/mainController');

var app = express();
app.use(bodyparser.urlencoded({
    extended: true
}));
app.use(bodyparser.json());

app.set('views', path.join(__dirname, '/views/'));
app.engine('ejs', expresshand({
    extname: 'ejs',
    defaultLayout: 'indexLayout',
    layoutsDir: __dirname + '/views/layouts/'
}));

app.set('view engine', 'ejs');
const port_decoute = process.env.PORT || 1990;

app.listen(port_decoute, () => {
    console.log('Le serveur Express est demare sur le port : '+port_decoute);
});

app.use('/', mainController);